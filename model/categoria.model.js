var database = require('../config/database.config');
var categoria = {};

categoria.selectAll = function(callback) {
  if(database) {
    database.query('SELECT * FROM Categoria', function(error, resultados) {
      if(error) throw error;
      callback(resultados);
    });
  }
}

categoria.find = function(idCategoria, callback) {
  if(database) {
    database.query('SELECT * FROM Categoria WHERE idCategoria=?', idCategoria, function(error, resultados){
      if(error) {
        throw error;
      } else {
        callback(resultados);
      }
    });
  }
}

categoria.insert = function(data, callback) {
  if(database) {
    var consulta = "CALL SP_addCategoria1(?)";
    database.query(consulta, data.nombreCategoria, function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"insertId": resultado.insertId});
      }
    });
  }
}

categoria.update = function(data, callback){
  if(database) {
    database.query('CALL SP_updateCategoria(?,?)',
    [data.idCategoria, data.nombreCategoria],
    function(error, resultado){
      if(error) {
        throw error;
      } else {
        callback(resultado[0]);
      }
    });
  }
}

categoria.delete = function(idCategoria, callback) {
  if(database) {
    var sql = "Call SP_deleteCategoria(?)";
    database.query(sql, idCategoria,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"Mensaje": "Eliminado"});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

module.exports = categoria;
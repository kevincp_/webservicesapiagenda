var database = require('../config/database.config');
var contacto = {};

contacto.selectAll = function(callback) {
  if(database) {
    database.query('SELECT * FROM vistaContacto', function(error, resultados) {
      if(error) throw error;
      callback(resultados);
    });
  }
}

contacto.find = function(idContacto, callback) {
  if(database) {
    database.query('SELECT * FROM Contacto WHERE idContacto= ?', idContacto, function(error, resultados){
      if(error) {
        throw error;
      } else {
        callback(resultados);
      }
    });
  }
}

contacto.insert = function(data, callback){
  if(database){
    database.query("Call SP_agregarContacto(?,?,?,?,?,?)",
      [data.nombreContacto, data.apellido, data.direccion, data.telefono, data.correo, data.idCategoria, data.idUsuario], function(error, resultado){
        if(error){
          throw error;
        }else{
          callback(null, {"insertId": resultado.insertId});
        }
      });
  }
}

contacto.update = function(data, callback){
  if(database){
    var sql = "Call SP_updateContacto(?,?,?,?,?,?,?)"
    database.query(sql, [data.idContacto, data.nombreContacto, data.apellido, data.direccion, data.telefono, data.correo, data.idCategoria],
       function(error, resultado){
        if(error){
          throw error;
        }else{
          callback(null, data);
        }
      });
  }
}

contacto.delete = function(idContacto, callback){
  if(database){
    var consulta = "Call SP_deleteContacto(?)";
    database.query(consulta, idContacto,
      function(error, resultado){
        if(error) {
        throw error;
      } else {
        callback(null, {"Mensaje": "Eliminado"});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

module.exports = contacto;
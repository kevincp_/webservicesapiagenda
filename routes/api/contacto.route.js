var express = require('express');
var contacto = require('../../model/contacto.model');
var router = express.Router();
var idUsuario = 1;

router.get('/contacto/', function(req, res, next) {
 // var idUsuario = req.usuario.idUsuario;
  contacto.selectAll(function(contactos) {
    if(typeof contactos !== 'undefined') {
      res.json(contactos);
    } else {
      res.json({"mensaje" : "No hay categorias"});
    }
  });
});

router.get('/contacto/:idContacto', function(req, res, next) {
  var idContacto = req.params.idContacto;
  contacto.find(idContacto, function(contactos) {
    if(typeof contactos !== 'undefined') {
      res.json(contactos.find(c => c.idContacto == idContacto));
    } else {
      res.json({"mensaje" : "No hay contactos"});
    }
  });
});



router.post('/contacto', function(req, res, next) {
  var data = {
    nombreContacto : req.body.nombreContacto,
    apellido : req.body.apellido,
    telefono : req.body.telefono,
    direccion : req.body.direccion,
    correo: req.body.correo,
    idCategoria : req.body.idCategoria
  };

  contacto.insert(data, function(resultado){
    if(resultado && resultado.affectedRows > 0) {
      res.json({
        estado: true,
        mensaje: "Se agrego el contacto"
      });
    } else {
        estado: false,
      res.json({"mensaje":"No se ingreso el contacto"});
    }
  });
});

router.put('/contacto/:idContacto', function(req, res, next){
  var data = {
    idContacto : req.params.idContacto,
    nombreContacto : req.body.nombreContacto,
    apellido : req.body.apellido,
    telefono : req.body.telefono,
    direccion : req.body.direccion,
    correo : req.body.correo,
    idCategoria : req.body.idCategoria
  }

  contacto.update(data, function(resultado){

    if(typeof resultado !== 'undefined') {
      res.json(resultado);
    } else {
      res.json({"mensaje":"No se pudo actualizar"});
    }
  });
});

router.delete('/contacto/:idContacto', function(req, res, next){
  var idContacto = req.params.idContacto;

  contacto.delete(idContacto, function(resultado){
    if(resultado && resultado.mensaje === "Eliminado") {
      res.json({"mensaje":"Se elimino el contacto correctamente"});
    } else {
      res.json({"mensaje":"Se elimino el contacto"});
    }
  });
});

module.exports = router;



var jwt = require('jsonwebtoken');
var services = {};
services.verificar = function (req, res, next) {
	console.log("Funcion para verificar token");
	var header = req.headers.authorization;
	if(typeof header !== undefined){
		var arrayHeader = header.split(" ");
		var token = arrayHeader[1];
		if(token){
			console.log("Si existe token");
			jwt.verify(token, 'corvus', function(error, decoded){
				if(error){
					if(error.name == "TokenExpiredError"){
						console.log("Se ha expirado");
					}
					return res.json({
						success: false,
						mensaje: 'Autenticacion fallida, expiro',
						error: err
					});
				} else {
					console.log("token decodificado");
					req.token = token;
					req.usuario = decoded;
					next();
				}
			});
		} else {
			console.log("No exite el token");
			res.json({
				estado: false,
				mensaje: "No existe el token"
			});
		}
	} else {
		console.log("No lleva la cabezera authorization");
		res.json({
			estado: false,
			mensaje: "No lleva la cabezera authorization"
		});
	}					
}

module.exports = services;